﻿// using SolarDefender.Components;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Rendering;
using Unity.Transforms;

namespace SolarDefender.Templates
{
	[UnityEngine.CreateAssetMenu(fileName = "NewShell", menuName = "Solar Defender/Entities/Shell", order = 0)]
	public class ShellTemplate : Template
	{
		public float3 translationValue = default;
		public float3 rotationValue = default;
		public float scaleValue = default;

		protected LocalToWorld _localToWorld = default;
		protected Translation _translation = default;
		protected Rotation _rotation = default;
		protected Scale _scale = default;

		[UnityEngine.Space]

		public float massValue = default;
		public float gravityFactorValue = default;
		public float colliderHeightValue = default;
		public float colliderRadiusValue = default;
		public float linearDampingValue = default;
		public float angularDampingValue = default;
		public float3 linearVelocityValue = default;
		public float3 angularVelocityValue = default;

		protected PhysicsMass _mass = default;
		protected PhysicsGravityFactor _gravityFactor = default;
		protected PhysicsDamping _damping = default;
		protected PhysicsVelocity _velocity = default;
		protected PhysicsCollider _collider = default;

		[UnityEngine.Space]

		public RenderMesh renderMesh = default;
		protected RenderBounds _renderBounds = default;

		// [UnityEngine.Space]

		// public VisualEffect movementVFX = default;
		// public VisualEffect hitVFX = default;

		// [UnityEngine.Space]

		// public DamageFactor damageFactor = default;

		protected override void InitializeComponents()
		{
			InitializeTransform();
			InitializePhysics();
			InitializeRendering();
		}

		protected void InitializeTransform()
		{
			quaternion rotationEuler = quaternion.Euler(rotationValue);

			_translation.Value = translationValue;
			_rotation.Value = rotationEuler;
			_scale.Value = scaleValue;

			// _localToWorld.Value = math.mul(new float4x4(rotationEuler, translationValue), float4x4.Scale(scaleValue));
		}

		protected unsafe void InitializePhysics()
		{
			BlobAssetReference<Collider> colliderBlobRef =
				CapsuleCollider.Create(
					new CapsuleGeometry()
					{
						Vertex0 = new float3(0f, colliderHeightValue / 2f, 0f),
						Vertex1 = new float3(0f, -colliderHeightValue / 2f, 0f),
						Radius = colliderRadiusValue
					}
				);

			Collider* colliderPtr = (Collider*)colliderBlobRef.GetUnsafePtr();
			_collider.Value = colliderBlobRef;

			_mass = PhysicsMass.CreateDynamic(
				colliderPtr->MassProperties,
				massValue
			);

			_gravityFactor.Value = gravityFactorValue;

			_damping.Linear = linearDampingValue;
			_damping.Angular = angularDampingValue;

			_velocity.Linear = linearVelocityValue;
			_velocity.Angular = math.mul(
				math.inverse(colliderPtr->MassProperties.MassDistribution.Transform.rot),
				angularVelocityValue
			);
		}

		protected void InitializeRendering()
		{
			_renderBounds.Value = renderMesh.mesh.bounds.ToAABB();
		}
	}
}