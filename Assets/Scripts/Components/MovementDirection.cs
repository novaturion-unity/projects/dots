using Unity.Entities;
using Unity.Mathematics;

namespace SolarDefender.Components
{
	public struct MovementDirection : IComponentData
	{
		public float3 value;

		public MovementDirection(float3 value)
		{
			this.value = value;
		}
	}
}